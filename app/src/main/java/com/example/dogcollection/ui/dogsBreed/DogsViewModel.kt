package com.example.dogcollection.ui.dogsBreed

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.map
import com.example.dogcollection.data.local.entity.DogEntity
import com.example.dogcollection.data.model.Dog
import com.example.dogcollection.data.repository.DogsRepository
import com.example.dogcollection.utils.EntityConverter.toDog
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@HiltViewModel
class DogsViewModel @Inject constructor(private val repository: DogsRepository) : ViewModel() {


    val pagingDataFlow: Flow<PagingData<Dog>>

    init {
        pagingDataFlow = getDogsFlow(order = "ASC")
            .cachedIn(viewModelScope)
    }
    fun getDogsFlow(order: String): Flow<PagingData<Dog>> {
        return repository.getDogs(order).map { pagingData ->
            pagingData.map { dogEntity ->
                dogEntity.toDog()
            }
        }.cachedIn(viewModelScope)
    }

}