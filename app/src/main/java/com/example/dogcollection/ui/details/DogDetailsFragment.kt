package com.example.dogcollection.ui.details

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.dogcollection.R
import com.example.dogcollection.databinding.FragmentDogDetailsBinding
import com.example.dogcollection.databinding.FragmentDogsBinding
import com.example.dogcollection.ui.dogsBreed.DogsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DogDetailsFragment : Fragment() {

    lateinit var binding: FragmentDogDetailsBinding
    private val args: DogDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDogDetailsBinding.inflate(inflater, container, false)
        val dog = args.dog
        Glide.with(binding.root).load(dog.image?.url).into(binding.ivDog)
        binding.tvBreedName.text = getString(R.string.dog_name_section,dog.name)
        binding.tvBreedGroup.text = getString(R.string.dog_group_section,dog.breedGroup  ?: getString(R.string.no_info))
        binding.tvOrigin.text = getString(R.string.dog_origin_section,if (dog.origin?.isNotEmpty() == true) dog.origin else getString(R.string.no_info))
        binding.tvTemperament.text = getString(R.string.dog_temperament_section,dog.temperament)
        return binding.root
    }


}