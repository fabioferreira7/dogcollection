package com.example.dogcollection.ui.details

import androidx.lifecycle.ViewModel
import com.example.dogcollection.data.repository.DogsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DogDetailsViewModel @Inject constructor(private val repository: DogsRepository) : ViewModel() {
    // TODO: Implement the ViewModel
}