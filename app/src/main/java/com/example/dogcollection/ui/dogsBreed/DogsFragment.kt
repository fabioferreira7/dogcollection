package com.example.dogcollection.ui.dogsBreed

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.dogcollection.data.adapter.DogPagerAdapter
import com.example.dogcollection.data.adapter.DogsLoadStateAdapter
import com.example.dogcollection.data.model.Dog
import com.example.dogcollection.databinding.FragmentDogsBinding
import com.example.dogcollection.utils.PreferencesHelper
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class DogsFragment : Fragment() {

    lateinit var binding: FragmentDogsBinding
    private val viewModel: DogsViewModel by viewModels()
    private val dogPagerAdapter: DogPagerAdapter = DogPagerAdapter(::onItemClicked)
    @Inject
    lateinit var preferencesHelper: PreferencesHelper
    private var currentOrder = ""
    private var isGridLayout = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDogsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    private fun onItemClicked(dog: Dog){
        val action = DogsFragmentDirections.actionDogsFragmentToDogDetailsFragment(dog)
        findNavController().navigate(action)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        currentOrder = preferencesHelper.getStoredTag("order")
        if (currentOrder.isEmpty()){
            val setOrder = if (binding.switchOrder.isChecked) "DESC" else "ASC"
            preferencesHelper.setStoredTag("order",setOrder)
            currentOrder = setOrder
        }
        else{
            binding.switchOrder.isChecked = currentOrder == "DESC"
        }
        binding.switchGrid.isChecked = isGridLayout
        setupRecyclerView()
        initListener()
        observeData()
    }

    private fun initListener() {
        binding.switchOrder.setOnCheckedChangeListener { _, isChecked ->
            val order = if (isChecked) "DESC" else "ASC"
            lifecycleScope.launch {
                binding.rvDogsBreed.visibility = View.GONE
                preferencesHelper.setStoredTag("order",order)
                currentOrder = order
                viewModel.getDogsFlow(currentOrder)
                dogPagerAdapter.refresh()
                binding.rvDogsBreed.scrollToPosition(0)
            }
        }
        binding.switchGrid.setOnCheckedChangeListener { _, isChecked ->
            isGridLayout = isChecked
            if (isGridLayout) {
                // Set grid layout manager to the RecyclerView
                binding.rvDogsBreed.layoutManager = GridLayoutManager(requireContext(), 2)
            } else {
                // Set list layout manager to the RecyclerView
                binding.rvDogsBreed.layoutManager = LinearLayoutManager(requireContext())
            }
            // Call notifyDataSetChanged on the adapter to update the layout
            dogPagerAdapter.setGridLayout(isGridLayout)
        }
    }


    private fun observeData() {
        lifecycleScope.launch {
            viewModel.pagingDataFlow
                .distinctUntilChanged()
                .collectLatest { pagingData ->
                    binding.rvDogsBreed.visibility = View.VISIBLE
                dogPagerAdapter.submitData(pagingData)
            }
        }
    }

    private fun setupRecyclerView() {
      if (isGridLayout){
          binding.rvDogsBreed.apply {
              layoutManager = GridLayoutManager(context,2)
              adapter = dogPagerAdapter
              itemAnimator = null
          }
      }else{
          binding.rvDogsBreed.apply {
              layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
              adapter = dogPagerAdapter
              itemAnimator = null
          }
      }

        val dogsLoadStateAdapter = DogsLoadStateAdapter { dogPagerAdapter.retry() }
        binding.rvDogsBreed.adapter = dogPagerAdapter.withLoadStateFooter(dogsLoadStateAdapter)
    }

}