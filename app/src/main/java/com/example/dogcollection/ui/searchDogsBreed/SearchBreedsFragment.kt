package com.example.dogcollection.ui.searchDogsBreed

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.dogcollection.data.adapter.DogListAdapter
import com.example.dogcollection.data.model.Dog
import com.example.dogcollection.databinding.FragmentSearchBreedsBinding
import com.example.dogcollection.utils.Resources
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchBreedsFragment : Fragment() {

    lateinit var binding: FragmentSearchBreedsBinding
    private val viewModel: SearchBreedsViewModel by viewModels()
    private lateinit var dogListAdapter: DogListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dogListAdapter = DogListAdapter(::onItemClicked)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchBreedsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        observeData()
    }

    private fun observeData() {
            viewModel.allDogBreeds
                .observe(viewLifecycleOwner) {
                    if (it.status == Resources.Status.SUCCESS) {
                        dogListAdapter.setItems(it.data!!)
                        initListener()
                    }
                }
    }

    private fun initListener() {

        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                    dogListAdapter.filter.filter(query)
                return false
            }

        })
    }


    private fun onItemClicked(dog: Dog) {
        val action = SearchBreedsFragmentDirections.actionDogSearchFragmentToDogDetailsFragment(dog)
        findNavController().navigate(action)
    }

    private fun setupRecyclerView() {
        binding.rvSearchBreeds.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = dogListAdapter
            itemAnimator = null
        }
    }
}