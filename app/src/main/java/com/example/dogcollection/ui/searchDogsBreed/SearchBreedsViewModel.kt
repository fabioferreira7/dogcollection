package com.example.dogcollection.ui.searchDogsBreed

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.map
import com.example.dogcollection.data.model.Dog
import com.example.dogcollection.data.repository.DogsRepository
import com.example.dogcollection.utils.EntityConverter.toDog
import com.example.dogcollection.utils.Resources
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchBreedsViewModel @Inject constructor(private val repository: DogsRepository) : ViewModel() {

    val allDogBreeds: MutableLiveData<Resources<List<Dog>>> = MutableLiveData()
    init {
        getAllBreeds()
    }

    private fun getAllBreeds() {
        viewModelScope.launch {
           allDogBreeds.value = repository.fetchDogsBreed(limit =null, page = null,null)
        }
    }

}