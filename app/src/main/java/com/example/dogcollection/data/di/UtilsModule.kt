package com.example.dogcollection.data.di

import android.content.Context
import com.example.dogcollection.utils.PreferencesHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class UtilsModule {

  @Provides
  @Singleton
  fun providePreferencesHelper(@ApplicationContext appContext: Context) : PreferencesHelper {
    return PreferencesHelper(appContext)
  }
}