package com.example.dogcollection.data.di

import android.content.Context
import androidx.room.Room
import com.example.dogcollection.data.local.AppDatabase
import com.example.dogcollection.data.local.dao.DogsDAO
import com.example.dogcollection.data.local.dao.RemoteKeysDao


import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {

  /**
   * Provides the instance of the AppDatabase.
   *
   * @param appContext The application context.
   * @return The instance of AppDatabase.
   */
  @Provides
  @Singleton
  fun provideDatabase(@ApplicationContext appContext: Context): AppDatabase {
    return Room.databaseBuilder(
      appContext,
      AppDatabase::class.java,
      "dogs_db"
    ).fallbackToDestructiveMigration().build()
  }

  /**
   * Provides the instance of the DogsDAO.
   *
   * @param appDatabase The AppDatabase instance.
   * @return The instance of DogsDAO.
   */
  @Provides
  fun provideDogsDAO(appDatabase: AppDatabase): DogsDAO {
    return appDatabase.dogsDAO()
  }
  @Provides
  fun provideRemoteKeysDAO(appDatabase: AppDatabase): RemoteKeysDao {
    return appDatabase.remoteKeysDAO()
  }
}