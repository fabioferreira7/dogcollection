package com.example.dogcollection.data.local.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.dogcollection.data.local.entity.DogEntity
import com.example.dogcollection.data.model.Dog

@Dao
interface DogsDAO {

    @Query("DELETE FROM DogEntity")
    suspend fun clearDogs()

    @Query("DELETE FROM DogImageEntity")
    suspend fun clearDogsImage()
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(dogs: List<DogEntity>?)
    @Query("SELECT * FROM DogEntity ORDER BY name DESC")
    fun getDogsDesc(): PagingSource<Int, DogEntity>

    @Query("SELECT * FROM DogEntity ORDER BY name ASC")
    fun getDogsAsc(): PagingSource<Int, DogEntity>

}