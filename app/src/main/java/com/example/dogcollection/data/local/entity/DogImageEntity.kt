package com.example.dogcollection.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "DogImageEntity")
data class DogImageEntity(
    @PrimaryKey
    val imageId: String,
    val width: Int?,
    val height: Int?,
    val url: String?
)