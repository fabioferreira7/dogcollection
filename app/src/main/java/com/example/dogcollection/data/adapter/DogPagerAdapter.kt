package com.example.dogcollection.data.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.dogcollection.data.model.Dog
import com.example.dogcollection.databinding.ItemDogGridBinding
import com.example.dogcollection.databinding.ItemDogListBinding

class DogPagerAdapter(
    private val onItemClicked: (dog: Dog) -> Unit
) : PagingDataAdapter<Dog, RecyclerView.ViewHolder>(DIFF_CALLBACK) {

    private var gridLayout = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEW_TYPE_GRID -> {
                val binding = ItemDogGridBinding.inflate(inflater, parent, false)
                DogGridViewHolder(binding)
            }
            else -> {
                val binding = ItemDogListBinding.inflate(inflater, parent, false)
                DogViewHolder(binding)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (gridLayout) {
            VIEW_TYPE_GRID
        } else {
            VIEW_TYPE_LIST
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val dog = getItem(position)
        dog?.let {
            when (holder) {
                is DogGridViewHolder -> holder.bind(it,onItemClicked)
                is DogViewHolder -> holder.bind(it,onItemClicked)
            }
        }
    }

    fun setGridLayout(isGridLayout: Boolean) {
        if (gridLayout != isGridLayout) {
            gridLayout = isGridLayout
            notifyDataSetChanged()
        }
    }

    inner class DogGridViewHolder(private val binding: ItemDogGridBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(dog: Dog, onItemClicked: (dog: Dog) -> Unit) {
            binding.tvDogName.text = dog.name
            Glide.with(binding.root).load(dog.image?.url).into(binding.ivDog)
            itemView.setOnClickListener {
                onItemClicked(dog)
            }
        }
    }

    inner class DogViewHolder(private val binding: ItemDogListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(dog: Dog, onItemClicked: (dog: Dog) -> Unit) {
            binding.tvDogName.text = dog.name
            Glide.with(binding.root).load(dog.image?.url).into(binding.ivDog)
            itemView.setOnClickListener {
                onItemClicked(dog)
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Dog>() {
            override fun areItemsTheSame(oldItem: Dog, newItem: Dog): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Dog, newItem: Dog): Boolean {
                return oldItem == newItem
            }
        }

        private const val VIEW_TYPE_LIST = 1
        private const val VIEW_TYPE_GRID = 2
    }
}