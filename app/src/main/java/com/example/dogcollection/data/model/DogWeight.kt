package com.example.dogcollection.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class DogWeight(
    @SerializedName("imperial") var imperial: String?,
    @SerializedName("metric") var metric: String?
) : Parcelable