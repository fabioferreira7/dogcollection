package com.example.dogcollection.data.mediator

import android.util.Log
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.example.dogcollection.data.local.AppDatabase
import com.example.dogcollection.data.local.entity.DogEntity
import com.example.dogcollection.data.local.entity.RemoteKeysEntity
import com.example.dogcollection.data.remote.DogsRemoteDataSource
import com.example.dogcollection.utils.EntityConverter.toDogEntityList
import com.example.dogcollection.utils.PreferencesHelper
import retrofit2.HttpException
import java.io.IOException
import java.lang.Exception
import javax.inject.Inject

private const val STARTING_PAGE_INDEX = 0

@OptIn(ExperimentalPagingApi::class)
open class DogsRemoteMediator(
    private val dogsRemoteDataSource: DogsRemoteDataSource,
    private val appDatabase: AppDatabase,
    val preferencesHelper:PreferencesHelper
) : RemoteMediator<Int, DogEntity>() {

    var resetPage = ""

    override suspend fun initialize(): InitializeAction {
        // Launch remote refresh as soon as paging starts and do not trigger remote prepend or
        // append until refresh has succeeded. In cases where we don't mind showing out-of-date,
        // cached offline data, we can return SKIP_INITIAL_REFRESH instead to prevent paging
        // triggering remote refresh.
        return InitializeAction.LAUNCH_INITIAL_REFRESH
    }

    override suspend fun load(loadType: LoadType, state: PagingState<Int, DogEntity>): MediatorResult {



        var page: Int = when (loadType) {
            LoadType.REFRESH -> {
                val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                remoteKeys?.nextKey?.minus(1) ?: STARTING_PAGE_INDEX
            }
            LoadType.PREPEND -> {
                val remoteKeys = getRemoteKeyForFirstItem(state)
                val prevKey = remoteKeys?.prevKey
                prevKey ?: return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
            }
            LoadType.APPEND -> {
                val remoteKeys = getRemoteKeyForLastItem(state)
                val nextKey = remoteKeys?.nextKey
                nextKey ?: return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
            }
        }
        try {

            if (resetPage.isEmpty()){
                resetPage = preferencesHelper.getStoredTag("order")
            }
            else{
                if (resetPage != preferencesHelper.getStoredTag("order")){
                    page = 0
                    appDatabase.remoteKeysDAO().clearRemoteKeys()
                    appDatabase.dogsDAO().clearDogs()
                    appDatabase.dogsDAO().clearDogsImage()
                    resetPage = preferencesHelper.getStoredTag("order")
                }
            }

            val apiResponse = dogsRemoteDataSource.fetchDogsBreed(state.config.pageSize,page,preferencesHelper.getStoredTag("order"))

            val dogs = apiResponse.data?.sortedBy { it.name }
            val endOfPaginationReached = dogs?.isEmpty() ?: true
            appDatabase.withTransaction {
                // clear all tables in the database
                if (loadType == LoadType.REFRESH) {
                    appDatabase.remoteKeysDAO().clearRemoteKeys()
                    appDatabase.dogsDAO().clearDogs()
                    appDatabase.dogsDAO().clearDogsImage()
                }
                val prevKey = if (page == STARTING_PAGE_INDEX) null else page - 1
                val nextKey = if (endOfPaginationReached) null else page + 1
                val keys = dogs?.map {
                    RemoteKeysEntity(dataId = it.id, prevKey = prevKey, nextKey = nextKey)
                }
                try {
                    appDatabase.remoteKeysDAO().insertAll(keys)
                    appDatabase.dogsDAO().insertAll(dogs?.toDogEntityList())
                }catch (e:Exception){
                    e.printStackTrace()
                }
            }
            return MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
        } catch (exception: IOException) {
            return MediatorResult.Error(exception)
        } catch (exception: HttpException) {
            return MediatorResult.Error(exception)
        }
    }

    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, DogEntity>): RemoteKeysEntity? {
        // Get the last page that was retrieved, that contained items.
        // From that last page, get the last item
        return state.pages.lastOrNull() { it.data.isNotEmpty() }?.data?.lastOrNull()
            ?.let { data ->
                // Get the remote keys of the last item retrieved
                appDatabase.remoteKeysDAO().remoteKeysDataId(data.id)
            }
    }

    private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, DogEntity>): RemoteKeysEntity? {
        // Get the first page that was retrieved, that contained items.
        // From that first page, get the first item
        return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()
            ?.let { data ->
                // Get the remote keys of the first items retrieved
                appDatabase.remoteKeysDAO().remoteKeysDataId(data.id)
            }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(
        state: PagingState<Int, DogEntity>
    ): RemoteKeysEntity? {
        // The paging library is trying to load data after the anchor position
        // Get the item closest to the anchor position
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { dataId ->
                appDatabase.remoteKeysDAO().remoteKeysDataId(dataId)
            }
        }
    }

}