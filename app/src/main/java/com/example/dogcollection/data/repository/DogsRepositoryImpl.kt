package com.example.dogcollection.data.repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.PagingSource
import com.example.dogcollection.data.local.AppDatabase
import com.example.dogcollection.data.local.dao.DogsDAO
import com.example.dogcollection.data.local.entity.DogEntity
import com.example.dogcollection.data.mediator.DogsRemoteMediator
import com.example.dogcollection.data.model.Dog
import com.example.dogcollection.data.remote.DogsRemoteDataSource
import com.example.dogcollection.utils.PreferencesHelper
import com.example.dogcollection.utils.Resources
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class DogsRepositoryImpl @Inject constructor(
    private val dogsRemoteDataSource: DogsRemoteDataSource,
    private val appDatabase: AppDatabase,
    private val preferencesHelper: PreferencesHelper,
    private val dogsDAO: DogsDAO) : DogsRepository
{

   val remoteMediator = DogsRemoteMediator(dogsRemoteDataSource, appDatabase,preferencesHelper)
    private val pagingSourceLiveData = MutableLiveData<() -> PagingSource<Int, DogEntity>>()

    override suspend fun fetchDogsBreed(limit: Int?, page: Int?,currentOrder: String?): Resources<List<Dog>> {
       return dogsRemoteDataSource.fetchDogsBreed(limit,page,currentOrder)
    }

    override fun getDogs(order: String): Flow<PagingData<DogEntity>> {
        val pagingSourceFactory: () -> PagingSource<Int, DogEntity> = if (order == "DESC") {
            { appDatabase.dogsDAO().getDogsDesc() }
        } else {
            { appDatabase.dogsDAO().getDogsAsc() }
        }

        pagingSourceLiveData.value = pagingSourceFactory


        @OptIn(ExperimentalPagingApi::class)
        val pager = Pager(
            config = PagingConfig(prefetchDistance = 10, pageSize = NETWORK_PAGE_SIZE, enablePlaceholders = true),
            remoteMediator = remoteMediator,
            pagingSourceFactory = { pagingSourceLiveData.value?.invoke()!! }
        )

        return pager.flow
    }

    companion object {
        const val NETWORK_PAGE_SIZE = 20
    }


}