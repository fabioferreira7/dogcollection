package com.example.dogcollection.data.local.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "DogEntity")
data class DogEntity(
    @PrimaryKey
    val id: Int,
    val name: String,
    val bredFor: String?,
    val breedGroup: String?,
    val lifeSpan: String?,
    val temperament: String?,
    val origin: String?,
    val referenceImageId: String?,
    val weightMetric: String?,
    val weightImperial: String?,
    val heightMetric:String?,
    val heightImperial:String?,
    @Embedded val dogImage: DogImageEntity?
)