package com.example.dogcollection.data.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.dogcollection.R
import com.example.dogcollection.data.model.Dog
import com.example.dogcollection.databinding.ItemDogInfoListBinding
import com.example.dogcollection.databinding.ItemDogListBinding
import java.util.Locale

class DogListAdapter(private val onItemClicked: (dog: Dog) -> Unit) :
    ListAdapter<Dog, DogListAdapter.DogViewHolder>(DogDiffCallback()), Filterable {

    private var originalList: List<Dog> = emptyList()
    private var filteredList: List<Dog> = emptyList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DogViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemDogInfoListBinding.inflate(inflater, parent, false)
        return DogViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DogViewHolder, position: Int) {
        val dog = getItem(position)
        holder.bind(dog)
    }

    inner class DogViewHolder(private val binding: ItemDogInfoListBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(dog: Dog) {
            Glide.with(binding.root).load(dog.image?.url).into(binding.ivDog)
            binding.tvDogName.text = itemView.context.getString(R.string.dog_name_section,dog.name)
            binding.tvDogGroup.text =  itemView.context.getString(
                R.string.dog_group_section,dog.breedGroup  ?:  itemView.context.getString(
                    R.string.no_info))
            binding.tvDogOrigin.text =  itemView.context.getString(
                R.string.dog_origin_section,if (dog.origin?.isNotEmpty() == true) dog.origin else  itemView.context.getString(
                    R.string.no_info))
            itemView.setOnClickListener {
                onItemClicked(dog)
            }
        }
    }

    private class DogDiffCallback : DiffUtil.ItemCallback<Dog>() {
        override fun areItemsTheSame(oldItem: Dog, newItem: Dog): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Dog, newItem: Dog): Boolean {
            return oldItem == newItem
        }
    }

    override fun getFilter(): Filter {
        return dogsFilter
    }

    fun setItems(dogs: List<Dog>) {
        originalList = dogs
        filteredList = dogs
        submitList(filteredList)
    }


    private val dogsFilter = object : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val filteredResults = mutableListOf<Dog>()
            if (constraint.isNullOrEmpty()) {
                filteredResults.addAll(originalList)
            } else {
                val filterPattern = constraint.toString().lowercase(Locale.getDefault()).trim()
                for (dog in originalList) {
                    if (dog.name.lowercase(Locale.getDefault()).contains(filterPattern)) {
                        filteredResults.add(dog)
                    }
                }
            }
            val filterResults = FilterResults()
            filterResults.values = filteredResults
            return filterResults
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            @Suppress("UNCHECKED_CAST")
            filteredList = results?.values as? List<Dog> ?: emptyList()
            submitList(filteredList)
        }
    }

}