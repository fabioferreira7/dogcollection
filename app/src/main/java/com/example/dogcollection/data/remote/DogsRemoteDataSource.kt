package com.example.dogcollection.data.remote

import com.example.dogcollection.data.model.Dog
import com.example.dogcollection.data.network.DogApiService
import com.example.dogcollection.utils.BaseGetResponse
import com.example.dogcollection.utils.Resources
import javax.inject.Inject

class DogsRemoteDataSource @Inject constructor(private val dogsApiService: DogApiService) :
    BaseGetResponse() {

    suspend fun fetchDogsBreed(limit: Int?, page: Int?,currentOrder:String?): Resources<List<Dog>> {
        return getResult { dogsApiService.fetchDogsBreed(limit,page,currentOrder) }
    }
}