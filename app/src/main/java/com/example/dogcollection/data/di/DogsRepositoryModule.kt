package com.example.dogcollection.data.di


import com.example.dogcollection.data.network.DogApiService
import com.example.dogcollection.data.repository.DogsRepository
import com.example.dogcollection.data.repository.DogsRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit


@Module
@InstallIn(SingletonComponent::class)
abstract class DogsRepositoryModule {

  @Binds
  abstract fun providesDogsRepository(impl: DogsRepositoryImpl) : DogsRepository

}



@InstallIn(SingletonComponent::class)
@Module
class DogsRepositoryModuleProvider {

  @Provides
  fun providesDogsApiService(retrofit: Retrofit) : DogApiService {
    return retrofit.create(DogApiService::class.java)
  }
}
