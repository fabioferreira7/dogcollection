package com.example.dogcollection.data.di

import android.content.Context
import com.example.dogcollection.R
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

  private val url = "https://api.thedogapi.com/v1/"

  /**
   * Provides an OkHttpClient instance with the API key header.
   *
   * @param appContext The application context provided by Dagger Hilt.
   * @return An OkHttpClient instance with the API key header added.
   */
  @Provides
  @Singleton
  fun provideOkHttpClient(@ApplicationContext appContext: Context) : OkHttpClient {
    return OkHttpClient.Builder()
      .followRedirects(true)
      .retryOnConnectionFailure(true)
      .addInterceptor { chain ->
        val originalRequest = chain.request()
        val newRequest = originalRequest.newBuilder()
          .header("x-api-key", appContext.getString(R.string.api_key))
          .build()
        chain.proceed(newRequest)
      }
      .build()
  }

  /**
   * Provides a Retrofit instance for making network requests.
   *
   * @param okHttpClient The OkHttpClient instance used for network communication.
   * @return A Retrofit instance for making network requests.
   */
  @Provides
  @Singleton
  fun provideRetrofit(okHttpClient: OkHttpClient) : Retrofit {
    return Retrofit.Builder()
      .baseUrl(url)
      .client(okHttpClient)
      .addConverterFactory(GsonConverterFactory.create())
      .build()
  }
}