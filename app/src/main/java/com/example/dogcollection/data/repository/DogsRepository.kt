package com.example.dogcollection.data.repository

import androidx.paging.PagingData
import com.example.dogcollection.data.local.entity.DogEntity
import com.example.dogcollection.data.model.Dog
import com.example.dogcollection.utils.Resources
import kotlinx.coroutines.flow.Flow

interface DogsRepository {

    suspend fun fetchDogsBreed(limit: Int?, page: Int?,currentOrder:String?): Resources<List<Dog>>
    fun getDogs(order: String): Flow<PagingData<DogEntity>>
}