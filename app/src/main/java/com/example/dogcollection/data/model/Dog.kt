package com.example.dogcollection.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class Dog(

    @SerializedName("id") var id: Int,
    @SerializedName("name") var name: String,
    @SerializedName("bred_for") var bredFor: String?= null,
    @SerializedName("breed_group") var breedGroup: String?= null,
    @SerializedName("life_span") var lifeSpan: String?= null,
    @SerializedName("temperament") var temperament: String?= null,
    @SerializedName("origin") var origin: String? = null,
    @SerializedName("reference_image_id") var referenceImageId: String?= null,
    @SerializedName("weight") var weight: DogWeight?= null,
    @SerializedName("height") var height: DogHeight?= null,
    @SerializedName("image") var image: DogImage?= null

):Parcelable