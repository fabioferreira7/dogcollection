package com.example.dogcollection.data.network

import com.example.dogcollection.data.model.Dog
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface DogApiService {
    @GET("breeds")
    suspend fun fetchDogsBreed(
        @Query("limit") limit: Int?,
        @Query("page") page: Int?,
        @Query("order") order: String?
    ): Response<List<Dog>>
}