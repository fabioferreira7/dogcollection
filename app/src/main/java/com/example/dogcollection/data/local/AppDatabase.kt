package com.example.dogcollection.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.dogcollection.data.local.dao.DogsDAO
import com.example.dogcollection.data.local.dao.RemoteKeysDao
import com.example.dogcollection.data.local.entity.DogEntity
import com.example.dogcollection.data.local.entity.DogImageEntity
import com.example.dogcollection.data.local.entity.RemoteKeysEntity

@Database(
    entities = [DogEntity::class,DogImageEntity::class,RemoteKeysEntity::class], version = 1
)

abstract class AppDatabase : RoomDatabase() {

    abstract fun dogsDAO(): DogsDAO
    abstract fun remoteKeysDAO(): RemoteKeysDao

}