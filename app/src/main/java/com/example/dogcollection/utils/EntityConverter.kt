package com.example.dogcollection.utils

import com.example.dogcollection.data.local.entity.DogEntity
import com.example.dogcollection.data.local.entity.DogImageEntity
import com.example.dogcollection.data.model.Dog
import com.example.dogcollection.data.model.DogHeight
import com.example.dogcollection.data.model.DogImage
import com.example.dogcollection.data.model.DogWeight

object EntityConverter {


    // Convert DogEntity to Dog
    fun DogEntity.toDog(): Dog {
        return Dog(
            weight = DogWeight(weightImperial, weightMetric),
            height = DogHeight(heightImperial, heightMetric),
            id = id,
            name = name,
            bredFor = bredFor,
            breedGroup = breedGroup,
            lifeSpan = lifeSpan,
            temperament = temperament,
            origin = origin,
            referenceImageId = referenceImageId,
            image = toDogImage()
        )
    }

    // Convert DogImageEntity to DogImage
    fun DogEntity.toDogImage(): DogImage {
        return DogImage(
            id = dogImage?.imageId.toString() ,
            width = dogImage?.width ?: 0, // Set the appropriate width value here
            height = dogImage?.height ?: 0, // Set the appropriate height value here
            url = dogImage?.url
        )
    }

    // Convert list of DogEntity to list of Dog
    fun List<DogEntity>.toDogList(): List<Dog> {
        return map { it.toDog() }
    }

    // Convert Dog to DogEntity
    fun Dog.toDogEntity(): DogEntity {
        return DogEntity(
            id = id,
            name = name,
            bredFor = bredFor,
            breedGroup = breedGroup,
            lifeSpan = lifeSpan,
            temperament = temperament,
            origin = origin,
            referenceImageId = referenceImageId,
            weightMetric = weight?.metric,
            weightImperial = weight?.imperial,
            heightMetric = height?.metric,
            heightImperial = height?.imperial,
            dogImage = toDogImageEntity()
        )
    }

    // Convert DogImage to DogImageEntity
    fun Dog.toDogImageEntity(): DogImageEntity? {
        return image?.let {
            DogImageEntity(
                imageId = it.id,
                width = it.width,
                height = it.height,
                url = it.url
            )
        }
    }

    // Convert list of Dog to list of DogEntity
    fun List<Dog>.toDogEntityList(): List<DogEntity> {
        return map { it.toDogEntity() }
    }
}