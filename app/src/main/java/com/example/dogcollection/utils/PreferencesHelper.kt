package com.example.dogcollection.utils

import android.content.Context

import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PreferencesHelper @Inject constructor(@ApplicationContext context: Context) {


    val prefs = context.getSharedPreferences("SPFile", Context.MODE_PRIVATE)

  fun getStoredTag(key: String): String {
    return prefs.getString(key, "")!!
  }
  fun setStoredTag(key: String, value: String) {
    prefs.edit().putString(key, value).commit()
  }
}