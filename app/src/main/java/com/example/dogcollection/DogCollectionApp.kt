package com.example.dogcollection

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class DogCollectionApp : Application()